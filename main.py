from flask import Flask, request, render_template, redirect, make_response
#все эти импорты выше относятся к библиотеке Flask, но лежат отдельно.
import os
import os.path #нужно для дадмина, чтобы он понимал из какой папки запускается программа
import sqlite3 # библиотека с БД. Самая компактная из достойных внимания
import hashlib # библиотека для создания MD5 хешей, которые хранятся в базе данных *пароли*
import smtplib # библиотека для отправки сообщений на почту


import random # нужен для создания нового случайного пароля


app = Flask(__name__, static_folder="static")
#cоздаемы гавную серверную переменную


class DataBase: # класс базаданных нужен для удобного обращения в базе данных.
    def __init__(self):
        self.cursor = None

    def converRawToUser(self, raw):
        users = []
        for rawUser in raw:
            user = User()
            user.id = int(rawUser[0])
            user.login = rawUser[1]
            user.password = rawUser[2]
            user.email = rawUser[3]
            users.append(user)
        return users

    def addUser(self, user):
        self.dbConnect()
        self.cursor.execute("""insert into users(USERNAME,PASSWORD,EMAIL) 
                values (?,?,?)""",
                            (user.login, user.password, user.email))
        self.connection.commit()
        self.dbDisconnect()
        # TODO Проверка наличия пользователя в БД

    def getUsers(self):
        self.dbConnect()
        self.cursor.execute("select * from users")
        rez = self.cursor.fetchall()
        self.dbDisconnect()
        return self.converRawToUser(rez)

    def getUserByLogin(self, login):
        self.dbConnect()
        self.cursor.execute("select * from users where USERNAME = ?", (login,))
        rez = self.cursor.fetchall()
        self.dbDisconnect()
        users = self.converRawToUser(rez)
        if users == []:
            return -1
        return users[0]
    def getAllUsers(self):
        self.dbConnect()
        self.cursor.execute("select * from users")
        rez = self.cursor.fetchall()
        self.dbDisconnect()
        print(rez)

    def getUserByEmail(self, email):
        self.dbConnect()
        print(email)
        self.cursor.execute("select * from users where EMAIL = ?", (email,))
        rez = self.cursor.fetchall()
        print(rez)
        self.dbDisconnect()
        users = self.converRawToUser(rez)
        if users == []:
            return -1
        return users[0]
    def dbUpdsateUserEmail(self, email, password):
        self.dbConnect()
        self.cursor.execute("Update users set password = ? where email = ?", (password,email))
        self.connection.commit()

        self.dbDisconnect()

    def dbConnect(self):
        try:
            self.connection = sqlite3.connect("mydatabase.db")
            self.cursor = self.connection.cursor()
        except Exception:
            self.dbInit()
            self.dbConnect()
            print("Ошибка подключения к Базе данных\n создание новоой базы", Exception)

    def dbInit(self): #эта функция должна быть вызвана вручную в том случае если отвутствует файл mydatabase.db
        if (self.cursor == None):
            self.dbConnect()
        str = ""
        for line in open("initDB.sql", "r"):
            str += line
        try:
            self.cursor.executescript(str)
            self.connection.commit()
        except sqlite3.OperationalError:
            print("Ошибка создания базы данных, скорее всего она уже существует")
        except Exception:
            print("Ошибка создания базы данных, не понятно что с ней")

    def dbDisconnect(self):
        self.connection.close()

db = DataBase()
class User:
    def __init__(self, l="", p="", e=""):
        self.login = l
        self.password = p
        self.email = e

@app.after_request
def add_header(r): #после каждого запроса добавляем кастомный хедер, для того, чтобы во время отладки не кешеировались различные файлы.
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

@app.route('/') #если пытаемся открыть путь "/" то
def logini(): #вызываектся эта функция.
    return render_template("login.html") #внутри страницы могли быть какие-то данные по типу логина пользователя, которые передаются с сервера.
    #поэтому используется функция render_template, но в итоге страницы остались пустыми, а менять эту работающую и так функцию не вижу симысла

@app.route('/index')#если пытаемся открыть путь "/index" то
def index():
    log = ""
    if request.cookies.get('login'): #првоеряем есть ли уже у пользователя т.н. пропуск к сайту.
        log = request.cookies.get('login')#он хранится в куках браузера
        users = db.getUsers()
        for u in users:
            hash_object = hashlib.md5(u.login.encode())# ключ - это хеш логина, если он совпадает, то все ок
            if hash_object.hexdigest() == log:#если среди всех пользователей из базы данных мы найдем пользователя у кторого будет подходящий ключ, то пропускаем его дальше
                return render_template("index.html")
    return redirect('/login?e=sessionDown', code=302)#в противном случае отправляем его на страницу с логином и пишем ему ошибку

@app.route('/login')
def login():
    res = make_response(render_template("login.html")   , 200)
    res.set_cookie('login', "")
    return res

@app.route('/loginAction', methods = ['POST', 'GET'])
def loginAction():
    userLogin = request.args.get('login')
    userPassword = request.args.get('password')
    u = db.getUserByLogin(userLogin)
    print(userPassword)
    print(u.password)
    if u != -1:
        if u.password == userPassword:
            res = redirect("/index", code=302)

            hash_object = hashlib.md5(userLogin.encode())
            res.set_cookie('login', hash_object.hexdigest())

            return res

    return redirect('/login?e=badLogin', code=302)

@app.route('/register')
def register():
    return render_template("registration.html")

@app.route('/regAction', methods = ['POST', 'GET'])
def registerAction():
    userLogin = request.args.get('login')
    userPasswod = request.args.get('password')
    userEmail = request.args.get('email')
    print(userLogin, userPasswod, userEmail)

    u = User(userLogin, userPasswod, userEmail)
    if db.getUserByLogin(userLogin)==-1:
        db.addUser(u)
        return redirect("/login?e=ok", code=302)
    return redirect("/register?e=exist", code=302)


@app.route('/bugReport')
def bugReport():

    return render_template("bugReport.html")

@app.route('/bugReportAction')
def bugReportAction():
    name = request.args.get('name')
    email = request.args.get('email')
    text = request.args.get('text')
    smtpObj = smtplib.SMTP_SSL('smtp.yandex.com', 465)
    smtpObj.login('TestDiplomSolovyova@yandex.ru', 'ksbthxwxpandzgwi')
    msg = 'From: %s\r\nTo: %s\r\nContent-Type: text/plain; charset="utf-8"\r\nSubject: %s\r\n\r\n' % (
        "TestDiplomSolovyova@yandex.ru", "TestDiplomSolovyova@yandex.ru", str(email))+text
    smtpObj.sendmail("TestDiplomSolovyova@yandex.ru", "TestDiplomSolovyova@yandex.ru", msg.encode('utf8'))
    smtpObj.quit()
    return redirect("/index", code=302)

@app.route('/index_EU')
def index_eu():
    log = ""
    if request.cookies.get('login'):
        log = request.cookies.get('login')
        users = db.getUsers()
        for u in users:
            hash_object = hashlib.md5(u.login.encode())
            if hash_object.hexdigest() == log:
                return render_template("index_eu.html")
    return redirect('/login?e=sessionDown', code=302)

@app.route('/restore')
def restore():
    return render_template("restore.html", code=200)


@app.route('/restoreAction')
def restoreAction():
    newPass = random.randint(10000000,100000000);
    db.getAllUsers()
    # print(request.args.get('email'))
    if request.args.get('email') :
        log = request.args.get('email')
        # print(str(log))
        user = db.getUserByEmail(str(log))
        # print(user)
        if user!=-1:
            ePass = hashlib.md5(str(newPass).encode())
            db.dbUpdsateUserEmail(user.email, '"'+str(ePass.hexdigest())+'"')
            user = db.getUserByEmail(str(log))
            print("новый пароль:",newPass)

            smtpObj = smtplib.SMTP_SSL('smtp.yandex.com',465)
            smtpObj.login('TestDiplomSolovyova@yandex.ru', 'ksbthxwxpandzgwi')
            msg = 'From: %s\r\nTo: %s\r\nContent-Type: text/plain; charset="utf-8"\r\nSubject: %s\r\n\r\n' % (
                "TestDiplomSolovyova@yandex.ru", user.email, str(newPass))
            smtpObj.sendmail("TestDiplomSolovyova@yandex.ru", user.email, msg.encode('utf8'))
            smtpObj.quit()
            return redirect('/login?e=restoreok', code=302)

    return redirect('/restore?e=badEmail', code=302)

@app.route('/instruction')
def instruction():
    return render_template("instruction.html", code=200)

@app.errorhandler(404)
def not_found_error(error):
    return "Эта страница находится еще в разработке  (("
@app.errorhandler(500)
def error500(error):
    return "Нам не удалось отправить письмо в автоматическом режие, обратитесь в службу поддержки"

if __name__ == "__main__":

    print(os.path.realpath(__file__))
    app.run()
