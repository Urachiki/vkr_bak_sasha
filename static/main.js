    //в этом файле находится основной код программы.
    
    let nums = 0; //счетчки объектов
    let arrowsDrawer1 = 0; //рисовалка, которую еще предстоит инициализировать
    let arrows = [] ;  //список всех стрелок
    let newArrow = []; //список, состоящий из 2 эелементов: отккуда идет стрека и куда
    let targetBlock = null;//текущий блок с которым взаимодействует пользователь

    function printDocument(documentId) {
        var doc = document.getElementById(documentId);

        //Wait until PDF is ready to print    
        if (typeof doc.print === 'undefined') {    
            setTimeout(function(){printDocument(documentId);}, 1000);
        } else {
            doc.print();
        }
    }


    
    function actionGrab(e){ //перемещение по плоскости, передается е - место, куда тыкнул пользователь
        targetBlock.style.top = e.pageY-targetBlock.clientHeight/2-10+"px";
        targetBlock.style.left = e.pageX-targetBlock.clientWidth/2-10+"px";
        arrowsDrawer1.redraw();
        //после каждого перемещение нельзя не перерисовывать стрелки.
    }

    // ниже 2 похожие функции, которые отвечают за создание стрелки.
    function createArrow2Step(e){ 
        for(let i = 0; i<nums; i++){
            if (e.target.classList.contains("b"+i)&&(newArrow[0]!=".b"+i)){
                newArrow[1] = ".b"+i;
                document.body.classList.remove("arrow2step");
                document.documentElement.removeEventListener("click", createArrow2Step);
                arrowsDrawer1.arrow(newArrow[0],newArrow[1]);
                arrows[arrows.length] = newArrow
                
                console.log(newArrow);
                messageDiv.innerHTML=""
                break;
            }
        }
    }
    //первый шаг зпоминает первый блок, на который тыкнул пользователь и передает управление второму шагу
    function createArrow1Step(e){
        messageDiv.innerHTML = "Выберите блок от которого нчнется стрелка"
        document.body.classList.add("arrow1step");
        for(let i = 0; i<nums; i++){
            if (e.target.classList.contains("b"+i)){
                newArrow[0] = ".b"+i;
                document.documentElement.removeEventListener("click", createArrow1Step);
                document.documentElement.addEventListener("click",createArrow2Step);
                messageDiv.innerHTML = "Выберите блок куда пйдет стрелка"
                document.body.classList.remove("arrow1step");
                document.body.classList.add("arrow2step");
            }
        }
    }
    //подготовительынй этап для создание стрелок
    function createArrow(e){
        newArrow = [];
        document.documentElement.addEventListener("click", createArrow1Step);
        console.log("createArrow()");
    }
    //ункция, которая отвечает за то, чтобы можно было писать текст.
    var actionChangeText = function(e){
        //внутри e.target (блок на который мы тыкнули дважды) создается и настраивается textarea 
        let text = document.createElement("textarea");
        text.value = e.target.innerHTML; //Берем старый текст внутри блока и запихиваем в textarea 
        e.target.innerHTML = "";//обнуляем текст блока, чтобы он не мешался 
        text.style.width=(e.target.clientWidth-30)+"px";//стилистические настройки textarea
        console.log(e.target.clientWidth)


        text.addEventListener("keydown", function(ee) {
            //каждыц раз по нажатию на кнопку проверям не нажали ли на Enter
            if(ee.key=="Enter")
                if(ee.target.value!="")
                    e.target.innerHTML = ee.target.value
                else
                    ee.target.remove()
            
        })
        text.addEventListener("focusout", function(ee) {
            //как только пользователь перестает писать и тыкает вдругую область, так же 
            //убираем блок textarea
            if(ee.target.value!="")
                e.target.innerHTML = ee.target.value
            else{
                ee.target.remove()
                document.removeEventListener("mousemove", actionGrab)
            }
        })
        e.target.appendChild(text);
        text.focus();//нужно, чтобы сразу можно было начинать писать
    }
    
    var blockConvertToItsystem = function(e){
        e.style.backgroundColor = "lightblue";
        e.classList.add("big_block");
    }
    var blockConvertToAnd = function(e) {
        e.classList.add("small_block");
    }

    btnCreateArrow.addEventListener("click", createArrow);
    btnCLearArrows.addEventListener("click", function(){
        arrowsDrawer1.CanvasStorage[2]=[];
        arrows = [];
        arrowsDrawer1.clear();
    })

   

    function startTrackMouseDown(e){
        targetBlock = e.target
        e.target.classList.add("onTop");
        document.addEventListener("mousemove", actionGrab)
    }
    function stopTrackMouseDown(e){
        e.target.classList.remove("onTop");
        document.removeEventListener("mousemove", actionGrab)
    }

    
    function createBlock(e,typeDiv){
        let block = document.createElement("div");
        block.classList.add("def_block");
        block.classList.add("b"+nums);
        nums++;
        targetBlock = block;
        //function evactionGrab(ee){actionGrab(ee,block);}
        block.addEventListener("mousedown",startTrackMouseDown); 
        block.addEventListener("mouseup",stopTrackMouseDown);
        block.addEventListener("contextmenu", function(e) {
            e.preventDefault()
            e.target.remove();
            arrowsDrawer1.redraw();
        });

        if(typeDiv!="or" && typeDiv!="and" && typeDiv!="xor"){
            block.classList.add("big_block");
            block.addEventListener("dblclick",actionChangeText);
        }
        else{
            block.classList.add("small_block");
            block.innerHTML=typeDiv
        }
        block.classList.add(typeDiv);
        document.getElementById("ex1-common-parent").appendChild(block);
        block.style.top = e.pageY-block.clientHeight/2-10+"px";
        block.style.left = e.pageX-block.clientWidth/2-10+"px";
        block.classList.add("onTop");
        document.addEventListener("mousemove", actionGrab)
        
    }
    function loadBlock(b,typeDiv,x,y,text){
        let block = document.createElement("div");
        block.classList.add("def_block");
        block.classList.add(b);
        nums++;
        function evactionGrab(ee){actionGrab(ee,block);}
        block.addEventListener("mousedown",startTrackMouseDown); 
        block.addEventListener("mouseup",stopTrackMouseDown);
        block.addEventListener("contextmenu", function(e) {
            e.preventDefault()
            e.target.remove();
            arrowsDrawer1.redraw(); 
        });

        if(typeDiv!="iror" && typeDiv!="irand" && typeDiv!="irxor" && typeDiv!="arrowPoint"){
            block.classList.add("big_block");
            block.addEventListener("dblclick",actionChangeText);
            block.innerHTML = text;
            block.classList.add(typeDiv);
        }
        else if(typeDiv=="arrowPoint")
            block.classList.add(typeDiv);
        else{
            block.classList.add("small_block");
            if(typeDiv=="iror")
                block.innerHTML="or";
            else if(typeDiv=="irxor")
                block.innerHTML="xor";

            else
            block.innerHTML="and";
            block.classList.add(typeDiv);
        }
        
        document.getElementById("ex1-common-parent").appendChild(block);
        block.style.top = y;
        block.style.left = x;

    }

    iraction.addEventListener("mousedown", function(e){ createBlock(e, "iraction")});
    irevent.addEventListener("mousedown", function(e){ createBlock(e, "irevent")});
    irrole.addEventListener("mousedown", function(e){ createBlock(e, "irrole")});
    irrisk.addEventListener("mousedown", function(e){ createBlock(e, "irrisk")});
    iriodata.addEventListener("mousedown", function(e){ createBlock(e, "iriodata")});
    iritsystem.addEventListener("mousedown", function(e){ createBlock(e, "iritsystem")});
    irand.addEventListener("mousedown", function(e){ createBlock(e, "and")});
    iror.addEventListener("mousedown", function(e){ createBlock(e, "or")});
    irxor.addEventListener("mousedown",function(e){ createBlock(e, "xor")});

    yourfiles.addEventListener("click", function(e){
        nums=0;
        arrow = [];
        let input = document.createElement('input');
        input.type = 'file';
        input.click();
        let reader = new FileReader()
        reader.onload = function() {
            arrowsDrawer1.CanvasStorage[2]=[];
            arrowsDrawer1.clear();
            arrows = [];
            oldDivs = document.getElementsByClassName("def_block");
            lenOldDivs = oldDivs.length;
            for(let i = 0; i<lenOldDivs; i++)
                oldDivs[0].remove();
            //тольк после удаления всех старых блоков можно добавлять новыйе 
            let mainData = reader.result.split("!!!\n");
            dataArrows = mainData[0].split("\n");
            dataDivs = mainData[1].split("\t\n\t");
            console.log(dataArrows);
            console.log(dataDivs);
    
            for(let i = 0; i<dataDivs.length-1; i++){
                let blockInfo = dataDivs[i].split(".!.");
                loadBlock(blockInfo[0],blockInfo[1], blockInfo[3], blockInfo[2],blockInfo[4] )
            }   
            for(let i = 0; i<dataArrows.length-1; i++){
                let tmp = dataArrows[i].split(" ");
                arrowsDrawer1.arrow(tmp[0],tmp[1]);
                arrows.push([tmp[0],tmp[1]])
                console.log(tmp);
            }
        }
        console.log(input);
        input.addEventListener("change",function(){reader.readAsText(input.files[0]);})
        
       
    })
    function downloadAsFile(data) {
        
        let file = new Blob([data], {type: 'application/json'});
        a.href = URL.createObjectURL(file);
        a.download = "example.txt";
        a.click();
        }

    downloadFile.addEventListener("click", function() {
        let a = document.createElement("a");
        let text =""; //"Данные, которые мы сохрянем в файт data.txt";
        for(let i = 0; i < arrows.length; i++)
            text+=arrows[i][0]+" "+arrows[i][1]+"\n";
        text+="!!!\n";
        let divs = document.getElementsByClassName("def_block");
        for(let i = 0; i < divs.length; i++){
            text+= divs[i].classList[1]+".!.";
            if( divs[i].classList[3] == "and")
                text+= "irand"+".!.";
            else if( divs[i].classList[3] == "or")
                text+= "iror"+".!.";
            else if( divs[i].classList[3] == "xor")
                text+= "irxor"+".!.";
            else if( divs[i].classList[2] == "arrowPoint")
                text+= "arrowPoint"+".!.";
            else
                text+= divs[i].classList[3]+".!.";
            text+= divs[i].style.top+".!.";
            text+= divs[i].style.left+".!.";
            text+= divs[i].innerHTML;
            text+="\t\n\t"
            
        }
        let myData = 'data:application/txt;charset=utf-8,' + encodeURIComponent(text);
        a.href = myData;
        a.download = 'data.txt';
        a.click();
    });

    function check3points(currX, currY, p1X, p1Y, p2X, p2Y){
        
        console.log([currX, currY, p1X, p1Y, p2X, p2Y])
        dxc = currX - p1X;
        dyc = currY - p1Y;

        dxl = p2X - p1X;
        dyl = p2Y - p1Y;

        cross = dxc * dyl - dyc * dxl;
        console.log(cross)
        if (-1000<cross && cross<1000 )
            if((p1X<currX && currX<p2X)||(p2X<currX && currX<p1X))
                return true;
        return false;
    }

    function arraysEqual(_arr1, _arr2) {

        if (!Array.isArray(_arr1) || ! Array.isArray(_arr2) || _arr1.length !== _arr2.length)
        return false;

        var arr1 = _arr1.concat().sort();
        var arr2 = _arr2.concat().sort();

        for (var i = 0; i < arr1.length; i++) {

            if (arr1[i] !== arr2[i])
                return false;

        }

        return true;

    }

    function getBlockXY(block){
        let p1x = parseInt(block.style.left.replace("px",""))+7;
        p1x+= block.offsetWidth/2;
        let p1y = parseInt(block.style.top.replace("px",""))+7;
        p1y+= block.offsetHeight/2;
        return [p1x, p1y];
    }

    function remove1Arrow(e){
        document.body.classList.remove("arrow1step");
        let currX=e.pageX
        let currY=e.pageY

        for(let i = 0; i<arrows.length; i++){

            let p1 = document.getElementsByClassName(arrows[i][0].replace(".",""))[0];
            let p2 = document.getElementsByClassName(arrows[i][1].replace(".",""))[0];

            let p1x = parseInt(p1.style.left.replace("px",""))+7;
            p1x+= p1.offsetWidth/2;
            let p1y = parseInt(p1.style.top.replace("px",""))+7;
            p1y+= p1.offsetHeight/2;
            
            let p2x = parseInt(p2.style.left.replace("px",""))+7;
            p2x+= p2.offsetWidth/2;
            let p2y = parseInt(p2.style.top.replace("px",""))+7;
            p2y+= p2.offsetHeight/2;


            if(check3points(currX,currY,p1x,p1y,p2x,p2y)){
                let targetIndex = arrowsDrawer1.CanvasStorage[2].findIndex(item => arraysEqual(item,[arrows[i][0],arrows[i][1],undefined]))
                let r1 = arrows[i][0];
                let r2 = arrows[i][1]
                arrowsDrawer1.CanvasStorage[2].splice(targetIndex, 1);
                arrowsDrawer1.redraw();
                

                arrows.splice(i,1);
                document.getElementById("ex1-common-parent").removeEventListener("click",remove1Arrow);
                console.log([r1,r2]);
                return [r1,r2];
                   
            }
        }
        document.getElementById("ex1-common-parent").removeEventListener("click",remove1Arrow);
        
    }


    btnCLear1Arrow.addEventListener("click", function(){
        document.getElementById("ex1-common-parent").addEventListener("click",remove1Arrow);
        document.body.classList.add("arrow1step");
    })

    btnCLearAll.addEventListener("click", function(e){
        btnCLearArrows.click();
        nums=0;
        arrow = [];
        let arr = document.getElementsByClassName("def_block");
        let len = arr.length
        for(let i = 0; i < len; i++){
            arr[0].remove();
        }
    })
    
    logout.addEventListener("click",function(){
        window.location.href = "/login";
    });
    instruction.addEventListener("click",function(){
        window.location.href = "/instruction";
    });
    helper.addEventListener("click",function(){
        window.location.href = "/bugReport";
    });
   

    function create2Arrows(from , too,e){
        let block = document.createElement("div");
        block.classList.add("def_block");
        block.classList.add("b"+nums);
        block.classList.add("arrowPoint");
        
        block.addEventListener("mousedown",startTrackMouseDown); 
        block.addEventListener("mouseup",stopTrackMouseDown);
        document.getElementById("ex1-common-parent").appendChild(block);
        block.style.top = e.pageY+"px";
        block.style.left = e.pageX+"px";

        arrowsDrawer1.arrows([[from, ".b"+nums, undefined],[".b"+nums, too , undefined]])
       
        arrows[arrows.length] = [from, ".b"+nums];
        arrows[arrows.length] = [".b"+nums, too];
        
        nums++;
        block.addEventListener("contextmenu", function(e) {
            e.preventDefault()
            e.target.remove();
            arrowsDrawer1.redraw();
        });
        arrowsDrawer1.redraw(); 

        
        return block
    }

    document.getElementById("ex1-common-parent").addEventListener("dblclick",function(e){
        let arr = remove1Arrow(e);
        
        if(arr!=undefined){
            let block = create2Arrows(arr[0],arr[1],e);
            console.log(arr);
           
            
        }
        
    });



    document.addEventListener("DOMContentLoaded",function(e){
     
        let drawDiv = document.getElementById("ex1-common-parent");
        drawDiv.style.height=document.documentElement.clientHeight+5000+"px";
        drawDiv.style.width=document.documentElement.clientWidth+5000+"px";
        arrowsDrawer1 = $cArrows('#ex1-common-parent');
        window.scrollTo((5000)/2, (5000)/2)
        window.onunload=function(e){e.preventDefoult();}
    })
